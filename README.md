# MTFS

[![dependency status](https://deps.rs/repo/gitlab/mtfs-rs/mtfs/status.svg)](https://deps.rs/repo/gitlab/mtfs-rs/mtfs) 
[![pipeline status](https://gitlab.com/mtfs-rs/mtfs/badges/master/pipeline.svg)](https://gitlab.com/mtfs-rs/mtfs/commits/master)
[![coverage report](https://gitlab.com/mtfs-rs/mtfs/badges/master/coverage.svg)](https://gitlab.com/mtfs-rs/mtfs/commits/master)

Multi Tier File System Core

## Installation


## Example


## Versioning

This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html) 
and use [git flow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow) branching model.

## Roadmap

Currently this is only a name reservation crate. First release expected for beginning of 2021. 
This project is a part of a Master thesis.

## License & Copyright

Copyright 2019 David Wittwer

Licensed under the terms of [GNU General Public License Version 3 or later](http://www.gnu.org/licenses/gpl.html). 
For full details about the license, please check the [`LICENSE`](LICENSE) file.

## Contribution

See [contribution guide](CONTRIBUTING.md)

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you, as defined in the GNU GPL V3 shall be
licensed as above, without any additional terms or conditions.